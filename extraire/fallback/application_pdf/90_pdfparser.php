<?php

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * Tester si cette méthode d'extraction est disponible
 **/
function extraire_fallback_application_pdf_90_pdfparser_test_dist() {
	if (!class_exists('\\Smalot\\PdfParser\\Parser')) {
		include_once _DIR_PLUGIN_EXTRAIREDOC . 'vendor/autoload.php';
	}

	if (!class_exists('\\Smalot\\PdfParser\\Parser')) {
		return false;
	}

	return true;
}

/**
 * Extraire le contenu pour le mime type pdf
 *
 *
 * @param $fichier le fichier à traiter
 * @return Scontenu le contenu brut
 **/
function extraire_fallback_application_pdf_90_pdfparser_extraire_dist($fichier) {
	$infos = ['contenu' => false];
	$contenu = '';

	// verifier la memoire disponible : on a besoin de 3 fois la taille du fichier (estimation)
	// TODO : verifier cette estimation pour ce parser
	include_spip('inc/extrairedoc');
	if (!extrairedoc_verifier_memoire_disponible(3 * filesize($fichier))) {
		return null;
	}

	if (!class_exists('\\Smalot\\PdfParser\\Parser')) {
		include_once _DIR_PLUGIN_EXTRAIREDOC . 'vendor/autoload.php';
	}

	$parser = new \Smalot\PdfParser\Parser();

	//Tenter de lire le pdf
	try {
		set_time_limit(0);
		$pdf = $parser->parseFile($fichier);
	} catch (Throwable $e) {
		spip_log("extraire_fallback_application_pdf_90_pdfparser_extraire_dist: Erreur lors de la lecture du PDF $fichier : " . $e->getMessage(), 'extrairedoc' . _LOG_ERREUR);
		//Pour toute exception on s'arrete et on retourne un contenu vide
		//Les cas de figure sont entre autre les fichiers mal formés ou signés
		return null;
	}

	// Récupérer les pages
	try {
		$pages = $pdf->getPages();
	} catch (Throwable $e) {
		spip_log("extraire_fallback_application_pdf_90_pdfparser_extraire_dist: Echec lors de la récupération des pages du PDF $fichier : " . $e->getMessage(), 'extrairedoc' . _LOG_ERREUR);
		//si on ne peut extraire le texte on passe à la page suivante
		return null;
	}

	// Parcourir les pages et extraire le contenu textuel
	foreach ($pages as $page) {
		try {
			$contenu .= $page->getText();
		} catch (Throwable $e) {
			spip_log("extraire_fallback_application_pdf_90_pdfparser_extraire_dist: Erreur lors de la récupération d'une page du PDF $fichier : " . $e->getMessage(), 'extrairedoc' . _LOG_ERREUR);
			//si on ne peut extraire le texte on passe à la page suivante
		}
	}

	//Libérer les ressources
	unset($parser);
	unset($loader);

	// Si on a trouvé du texte
	if ($contenu) {
		$infos['contenu'] = $contenu;
	}

	return $infos;
}
