# Extracteur de document

Ce plugin fourni une API pour extraire le contenu à partir de documents.

## Exemple d'utilisation

```php
include_spip('base/abstract_sql');
$document = sql_fetsel('*', 'spip_documents', 'id_document=' . intval($id_document));
$extraire_document = charger_fonction('extraire_document', 'inc');
$infos = $extraire_document($document);
```

## Fonctions d'extractions

La fonction `$extraire_document` utilise le mime-type du document pour chercher une fonction extracteur pour ce type de document.

Par défaut le plugin fournit 3 extracteurs pour les fichiers PDF uniquement :
* un extracteur `tika_server` basé sur un serveur Tika (non fourni, vous devez installer un serveur Tika et configurer son URL dans la configuration du plugin)
* un extracteur `pdfparser` basé sur la https://github.com/smalot/pdfparser/ (fourni en `vendor/` dans le plugin)
* un extracteur `pdfexec` basé sur une commande exec de votre choix, à définir via la constante `_EXTRACT_PDF_EXEC`


Les extracteurs sont recherchés dans les dossiers `extraire/` du path SPIP, selon la préséance suivante
* `extraire/defaut/{$chemin_mime}/`
* `extraire/defaut/`
* `extraire/fallback/{$chemin_mime}/`
* `extraire/fallback/`

Ils sont conventionnellement préfixés d'un rang sur 2 chiffres, qui permet de fixer une priorité d'utilisation.

Chaque extracteur doit fournir une fonction test qui permet de tester si le dit extracteur peut-être utilisé (valide pour le mime cherché, configuré, libs disponibles...)

Vous pouvez donc fournir vos propres extracteurs dans un plugin complémentaire ou dans le dossier `squelettes/`
