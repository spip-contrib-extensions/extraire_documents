<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/fulltext/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'configurer_titre' => 'Configurer l’extraction de documents',
	'configurer_tika_host_label' => 'Hôte du serveur Tika',
	'configurer_tika_port_label' => 'Port du serveur Tika',
);
